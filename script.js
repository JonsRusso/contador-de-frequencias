/*One Fish, Two Fish, Red Fish, Blue Fish By Dr. Seuss
One fish Two fish Red fish Blue fish.
Black fish Blue fish Old fish New fish.
This one has a little star. This one has a little car. Say! What a lot
Of fish there are.
Yes. Some are red. And some are blue. Some are old. And some are new.
Some are sad.
And some are very, very bad.
Why are they Sad and glad bad? I do not know.
Go ask your dad.
Some are thin. And some are fato. The fat one has A yellow hat.
From there to here, from here to there, Funny things
Are everywhere.
Here are some Who like to run. They run for fun In the hot, hot sun. */


document.getElementById("contar").addEventListener('click', comparar);

function comparar() {
  let texto = document.getElementById("entradatexto").value;
  let letra;
  const contador = {};
  texto = texto.toLowerCase();
  texto = texto.replace(/[^a-z'\s]+/g, "");
  for (let i = 0; i < texto.length; i++) {
    letra = texto[i];
    if(contador[letra] === undefined){
      contador[letra] = 1;
    }else{
      contador[letra]++;
    }
  }
  let palavra = texto.split(/\s/);
  let algo ='';
  for (let i = 0; i < palavra.length; i++){
    algo = palavra[i]
    if(contador[algo] === undefined){
      contador[algo] = 1;
    }else{
      contador[algo]++;
    }
    }
    for(let algo in contador){
      const span = document.createElement("span"); 
      const textContent = document.createTextNode(`"${algo}": ${contador[algo]},`); 
      span.appendChild(textContent); 
      document.getElementById("palavras").appendChild(span);
  }
}